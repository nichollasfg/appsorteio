package br.edu.ifsc.appSorteio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.style.CharacterStyle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void sortear(View view) {
        EditText idTexto1=findViewById(R.id.texto1);
        EditText idTexto2=findViewById(R.id.texto2);

        String Strnumero1=idTexto1.getText().toString();
        int numero1=Integer.parseInt(Strnumero1);
        String Strnumero2=idTexto2.getText().toString();
        int numero2=Integer.parseInt(Strnumero2);
        int maior;
        int menor;
        if(numero1>numero2){
            maior=numero1;
            menor=numero2;
        }else{
            maior=numero2;
            menor=numero1;
        }

        Random random=new Random();
        int nSorteado=random.nextInt(maior);
        String convertNumSort=Integer.toString(nSorteado);
        TextView text= findViewById(R.id.textViewTexto);
        text.setText(nSorteado);
    }
}
